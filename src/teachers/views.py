from django.db.models import Q
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from teachers.forms import TeacherForm
from teachers.models import Teacher


# Create your views here.
@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_teachers(request: HttpRequest, params) -> HttpResponse:
    teachers = Teacher.objects.all()

    search_fields = [
        "first_name",
        "last_name",
        "email",
    ]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            teachers = teachers.filter(or_filter)
        else:
            teachers = teachers.filter(**{param_name: param_value})
    return render(request, template_name="teachers/get_teachers.html", context={"teachers": teachers})


@csrf_exempt
def create_teachers(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = TeacherForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:get_teachers"))
    elif request.method == "GET":
        form = TeacherForm()

    return render(request, template_name="teachers/create_teachers.html", context={"form": form})


@csrf_exempt
def update_teachers(request: HttpRequest, pk: int) -> HttpResponse:
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)
    if request.method == "POST":
        form = TeacherForm(request.POST, request.FILES, instance=teacher)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:get_teachers"))
    elif request.method == "GET":
        form = TeacherForm(instance=teacher)

    return render(request, template_name="teachers/update_teachers.html", context={"form": form})


@csrf_exempt
def delete_teachers(request: HttpRequest, pk: int) -> HttpResponse:
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)
    if request.method == "POST":
        teacher.delete()
        return HttpResponseRedirect(reverse("teachers:get_teachers"))
    elif request.method == "GET":
        pass

    return render(request, template_name="teachers/delete_teachers.html", context={"teacher": teacher})


@csrf_exempt
def teacher_group(request: HttpRequest, pk: int) -> HttpResponse:
    teacher = get_object_or_404(Teacher.objects.all(), pk=pk)
    groups = teacher.group.all()

    return render(request, template_name="teachers/teachers_group.html", context={
        "teacher": teacher, "groups": groups}, )
