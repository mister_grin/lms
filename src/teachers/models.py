from datetime import datetime

from django.db import models
from faker import Faker


class Teacher(models.Model):
    avatar = models.ImageField(upload_to="media/", null=True, blank=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=100)
    birth_date = models.DateField(null=True, blank=True)
    email = models.EmailField(max_length=150)
    phone_number = models.CharField(max_length=30, null=True, blank=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    group = models.ManyToManyField("groups.Group")

    @classmethod
    def generate_teachers(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            teacher = Teacher(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                birth_date=faker.date_of_birth(minimum_age=18, maximum_age=70),
                email=faker.email(),
                phone_number=faker.phone_number(),
                city=faker.city(),
            )
            teacher.save()

    def age(self) -> int:
        return datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name} {self.last_name}, {self.phone_number}, {self.email}, {self.pk}"
