# Generated by Django 4.2.11 on 2024-04-23 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("teachers", "0008_teacher_avatar"),
    ]

    operations = [
        migrations.AlterField(
            model_name="teacher",
            name="avatar",
            field=models.ImageField(blank=True, null=True, upload_to="static/img"),
        ),
    ]
