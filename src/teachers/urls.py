from django.urls import path

from teachers.views import get_teachers, create_teachers, update_teachers, delete_teachers, teacher_group

app_name = "teachers"

urlpatterns = [
    path("", get_teachers, name="get_teachers"),
    path("create/", create_teachers, name="create_teachers"),
    path("update/<int:pk>", update_teachers, name="update_teachers"),
    path("delete/<int:pk>", delete_teachers, name="delete_teachers"),
    path("teacher-groups/<int:pk>", teacher_group, name="teacher_group")
]
