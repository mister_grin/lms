from django.forms import ModelForm
from teachers.models import Teacher


class TeacherForm(ModelForm):
    class Meta:
        model = Teacher
        fields = [
            "avatar",
            "first_name",
            "last_name",
            "email",
            "phone_number",
            "city",
            "birth_date",
            "group",
        ]

    @staticmethod
    def normalize_text(text):
        return text.strip().capitalize()

    def clean_first_name(self):
        return self.normalize_text(self.cleaned_data["first_name"])

    def clean_last_name(self):
        return self.normalize_text(self.cleaned_data["last_name"])

    def clean_city(self):
        return self.normalize_text(self.cleaned_data["city"])
