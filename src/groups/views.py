from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from groups.forms import GroupForm
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args
from django.db.models import Q
from groups.models import Group
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect


@use_args(
    {
        "name": fields.Str(
            required=False,
        ),
        "status": fields.Str(
            required=False,
        ),
        "duration_study_months": fields.Int(required=False),
        "start_date": fields.Int(required=False),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_groups(request: HttpRequest, params) -> HttpResponse:
    groups = Group.objects.all()
    search_fields = ["name", "duration_study_months", "status", "start_date"]
    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            groups = groups.filter(or_filter)
        else:
            groups = groups.filter(**{param_name: param_value})

    return render(request, template_name="groups/get_groups.html", context={"groups": groups})


@csrf_exempt
def create_groups(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/groups")
    elif request.method == "GET":
        form = GroupForm()

    return render(request, template_name="groups/create_group.html", context={"form": form})


@csrf_exempt
def update_groups(request: HttpRequest, pk: int) -> HttpResponse:
    group = get_object_or_404(Group.objects.all(), pk=pk)
    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:get_groups"))
    elif request.method == "GET":
        form = GroupForm(instance=group)

    return render(request, template_name="groups/update_groups.html", context={"form": form})


@csrf_exempt
def delete_groups(request: HttpRequest, pk: int) -> HttpResponse:
    group = get_object_or_404(Group.objects.all(), pk=pk)

    if request.method == "POST":
        group.delete()
        return HttpResponseRedirect(reverse("groups:get_groups"))
    elif request.method == "GET":
        pass

    return render(request, template_name="groups/delete_groups.html", context={"group": group})
