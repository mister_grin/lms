from django.core.exceptions import ValidationError
from django.forms import ModelForm
from groups.models import Group


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = [
            "name",
            "start_date",
            "max_quantity_teachers",
            "duration_study_months",
            "max_quantity_students",
            "status",
        ]

    @staticmethod
    def normalize_text(text):
        return text.strip().capitalize()

    def clean_name(self):
        return self.normalize_text(self.cleaned_data["name"])

    def clean_status(self):
        return self.normalize_text(self.cleaned_data["status"])

    def clean_start_date(self):
        start_date = self.cleaned_data["start_date"]
        if start_date is None:
            raise ValidationError("Enter a date")
        return start_date
