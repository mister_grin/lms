from django.urls import path

from groups.views import get_groups, create_groups, update_groups, delete_groups

app_name = "groups"

urlpatterns = [
    path("", get_groups, name="get_groups"),
    path("create/", create_groups, name="create_groups"),
    path("update/<int:pk>", update_groups, name="update_groups"),
    path("delete/<int:pk>", delete_groups, name="delete_groups"),
]
