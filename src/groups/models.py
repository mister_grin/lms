import random

from django.core.validators import MaxValueValidator
from django.db import models
from faker import Faker


class Group(models.Model):
    STATUS_GROUP = [
        ("Active", "active"),
        ("Completed", "completed"),
        ("Suspended", "suspended"),
    ]
    GROUP_NAME = [
        ("Python", "Python"),
        ("Java", "Java"),
        ("Frontend", "Frontend"),
        ("C++", "C++"),
        ("Php", "Php"),
        ("C#", "C#"),
    ]

    start_date = models.DateField(null=True, blank=True)
    max_quantity_students = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(20)])
    name = models.CharField(max_length=100, choices=GROUP_NAME, default=GROUP_NAME[0])
    max_quantity_teachers = models.PositiveIntegerField(default=1)
    status = models.CharField(max_length=20, choices=STATUS_GROUP, default="active")
    duration_study_months = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(24)])

    @classmethod
    def generate_groups(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            group = Group(
                start_date=faker.date(start_date="2021-01-01", end_date="2024-12-31"),
                quantity_students=faker.random_int(min_value=1, max_value=20),
                quantity_teachers=faker.random_int(min_value=1, max_value=3),
                name=random.choice([name[0] for name in cls.GROUP_NAME]),
                status=random.choice([status[0] for status in cls.STATUS_GROUP]),
                duration_study_months=faker.random_int(min_value=1, max_value=18),
            )
            group.save()

    def __str__(self):
        return (
            f"({self.pk}) {self.name}, {self.max_quantity_students} students, {self.start_date},"
            f"{self.duration_study_months} months, {self.max_quantity_teachers}"
        )
