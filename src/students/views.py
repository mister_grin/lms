from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from django.http import JsonResponse  # NOQA:F401
from django.shortcuts import render, get_object_or_404
from webargs.djangoparser import use_args
from django.db.models import Q

from students.forms import StudentForm
from students.models import Student
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect


def index(request: HttpRequest) -> HttpResponse:
    return render(request, template_name="index.html")


def view_404(request, exception):
    return render(request, template_name='404.html', status=404)


@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_students(request: HttpRequest, params) -> HttpResponse:
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})

    return render(request, template_name="students/get_students.html", context={"students": students})


@csrf_exempt
def create_students(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = StudentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm()

    return render(request, template_name="students/create_student.html", context={"form": form})


@csrf_exempt
def update_students(request: HttpRequest, pk: int) -> HttpResponse:
    student = get_object_or_404(Student.objects.all(), pk=pk)
    if request.method == "POST":
        form = StudentForm(request.POST, request.FILES, instance=student)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm(instance=student)

    return render(request, template_name="students/update_student.html", context={"form": form})


@csrf_exempt
def delete_students(request: HttpRequest, pk: int) -> HttpResponse:
    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        student.delete()
        return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        pass

    return render(request, template_name="students/delete_student.html", context={"student": student})


@csrf_exempt
def summary_students(request: HttpRequest, pk: int) -> HttpResponse:
    student = get_object_or_404(Student.objects.all(), pk=pk)
    return render(request, template_name="students/students_summary.html", context={"student": student})

