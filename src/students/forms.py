from django.forms import ModelForm
from students.models import Student


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = [
            "avatar",
            "first_name",
            "last_name",
            "email",
            "group",
            "summary",
        ]

    @staticmethod
    def normalize_text(text):
        return text.strip().capitalize()

    def clean_first_name(self):
        return self.normalize_text(self.cleaned_data["first_name"])

    def clean_last_name(self):
        return self.normalize_text(self.cleaned_data["last_name"])
