from django.contrib import admin  # NOQA:F401
from students.models import Student
# Register your models here.

admin.site.register(Student)
