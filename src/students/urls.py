from django.urls import path

from students.views import get_students, create_students, update_students, delete_students, summary_students

app_name = "students"

urlpatterns = [
    path("", get_students, name="get_students"),
    path("create/", create_students, name="create_students"),
    path("update/<int:pk>", update_students, name="update_students"),
    path("delete/<int:pk>", delete_students, name="delete_students"),
    path("summary-students/<int:pk>", summary_students, name="summary_students"),
]
