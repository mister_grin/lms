# Generated by Django 4.2.11 on 2024-04-20 14:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("students", "0003_student_group"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="student",
            name="group",
        ),
    ]
