import datetime


from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker


class Student(models.Model):
    avatar = models.ImageField(upload_to="media/", null=True, blank=True)
    first_name = models.CharField(max_length=120, validators=[MinLengthValidator(3)], null=True, blank=True)
    last_name = models.CharField(max_length=150, null=True, blank=True)
    email = models.EmailField(max_length=100)
    grade = models.PositiveIntegerField(default=0, null=True)
    birth_date = models.DateField(null=True, blank=True)
    summary = models.FileField(upload_to="files/", null=True, blank=True)
    group = models.ForeignKey("groups.Group", on_delete=models.CASCADE, default=13)

    @classmethod
    def generate_students(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            student = Student(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                birth_date=faker.date_of_birth(minimum_age=18, maximum_age=70),
                email=faker.email(),
            )
            student.save()

    def age(self) -> int:
        return datetime.datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name} {self.last_name}, {self.email}, {self.pk}"
